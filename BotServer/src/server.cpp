/*
 * This file is part of the OpenKinect Project. http://www.openkinect.org
 *
 * Copyright (c) 2010 individual OpenKinect contributors. See the CONTRIB file
 * for details.
 *
 * This code is licensed to you under the terms of the Apache License, version
 * 2.0, or, at your option, the terms of the GNU General Public License,
 * version 2.0. See the APACHE20 and GPL2 files for the text of the licenses,
 * or the following URLs:
 * http://www.apache.org/licenses/LICENSE-2.0
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you redistribute this file in source form, modified or unmodified, you
 * may:
 *   1) Leave this header intact and distribute it under the same terms,
 *      accompanying it with the APACHE20 and GPL20 files, or
 *   2) Delete the Apache 2.0 clause and accompany it with the GPL2 file, or
 *   3) Delete the GPL v2 clause and accompany it with the APACHE20 file
 * In all cases you must keep the copyright notice intact and include a copy
 * of the CONTRIB file.
 *
 * Binary distributions must follow the binary distribution requirements of
 * either License.
 */

#include <cstdlib>
#include <iostream>
#include <vector>
#include <libfreenect.hpp>

#if defined(__APPLE__)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <fstream>
#include <iostream>
#include <set>
#include <streambuf>
#include <string>

#include "boost/asio.hpp"
#include <boost/iostreams/filtering_streambuf.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>

using namespace boost::asio;

class Mutex
{
public:
    Mutex()
    {
        pthread_mutex_init(&m_mutex, NULL);
    }

    void lock()
    {
        pthread_mutex_lock(&m_mutex);
    }

    void unlock()
    {
        pthread_mutex_unlock(&m_mutex);
    }

    class ScopedLock
    {
    public:
        ScopedLock(Mutex &mutex) : _mutex(mutex)
        {
            _mutex.lock();
        }

        ~ScopedLock()
        {
            _mutex.unlock();
        }

    private:
        Mutex &_mutex;
    };

private:
    pthread_mutex_t m_mutex;
};


class MyFreenectDevice : public Freenect::FreenectDevice
{
public:
    MyFreenectDevice(freenect_context *_ctx, int _index)
        : Freenect::FreenectDevice(_ctx, _index),
          m_buffer_video(freenect_find_video_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_VIDEO_RGB).bytes),
          m_buffer_depth(freenect_find_depth_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_DEPTH_REGISTERED).bytes / 2),
          m_new_rgb_frame(false), m_new_depth_frame(false)
    {
        setDepthFormat(FREENECT_DEPTH_REGISTERED);
    }

    // Do not call directly, even in child
    void VideoCallback(void *_rgb, uint32_t timestamp)
    {
        Mutex::ScopedLock lock(m_rgb_mutex);
        uint8_t* rgb = static_cast<uint8_t*>(_rgb);
        copy(rgb, rgb+getVideoBufferSize(), m_buffer_video.begin());
        m_new_rgb_frame = true;
    }

    // Do not call directly, even in child
    void DepthCallback(void *_depth, uint32_t timestamp)
    {
        Mutex::ScopedLock lock(m_depth_mutex);
        uint16_t* depth = static_cast<uint16_t*>(_depth);
        copy(depth, depth+getDepthBufferSize()/2, m_buffer_depth.begin());
        m_new_depth_frame = true;
    }

    bool getRGB(std::vector<uint8_t> &buffer)
    {
        Mutex::ScopedLock lock(m_rgb_mutex);
        
        if (!m_new_rgb_frame)
            return false;

        buffer.swap(m_buffer_video);
        m_new_rgb_frame = false;

        return true;
    }

    bool getDepth(std::vector<uint16_t> &buffer)
    {
        Mutex::ScopedLock lock(m_depth_mutex);
        
        if (!m_new_depth_frame)
            return false;

        buffer.swap(m_buffer_depth);
        m_new_depth_frame = false;

        return true;
    }

private:
    Mutex m_rgb_mutex;
    Mutex m_depth_mutex;
    std::vector<uint8_t> m_buffer_video;
    std::vector<uint16_t> m_buffer_depth;
    bool m_new_rgb_frame;
    bool m_new_depth_frame;
};


Freenect::Freenect freenect;
MyFreenectDevice* device;

struct Point{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint16_t x;
    uint16_t y; 
    uint16_t z;
};

void frameUpdate(std::vector<uint16_t> &combined){
    static std::vector<uint8_t> rgb(640*480*3);
    static std::vector<uint16_t> depth(640*480);

    device->getRGB(rgb);
    device->getDepth(depth);

    device->updateState();
    Freenect::FreenectTiltState state = device->getState();
    double x;
    double y;
    double z;
    
    state.getAccelerometers(&x,&y,&z);

    std::cout << x << " " << y << " " << z << " " << std::endl; 

    float f = 595.f;

    for (int i = 0; i < 480*640; ++i){
        int d = i*6;   
        combined[d+0] = rgb[3*i+0]; // R
        combined[d+1] = rgb[3*i+1]; // G
        combined[d+2] = rgb[3*i+2]; // B
        combined[d+3] = (i%640 - (640-1)/2.f) * depth[i] / f;   // X = (x - cx) * d / fx
        combined[d+4] = (i/640 - (480-1)/2.f) * depth[i] / f;   // Y = (y - cy) * d / fy
        combined[d+5] = depth[i];                               // Z = d
    }
}


int main(int argc, char **argv)
{
    device = &freenect.createDevice<MyFreenectDevice>(0);
    device->startVideo();
    device->startDepth();

    using boost::asio::ip::tcp;
    boost::asio::io_context io_context;

    tcp::socket tcp_sock(io_context);
    tcp::resolver resolver(io_context);
    boost::asio::connect(tcp_sock, resolver.resolve("10.0.6.11", "9020"));

    while(1){
        static std::vector<uint16_t> combined(640*480*6);
        frameUpdate(combined);

        int size_total = 0;
        std::stringstream s;
        for (int i = 0; i < 480*640; ++i){ 
            int b = i * 6;
            s << combined[b] << "," << combined[b+1] << "," << combined[b+2]<< "," << combined[b+3]<< "," << combined[b+4]<< "," << combined[b+5];
        }

        namespace bio = boost::iostreams;

        std::stringstream compressed;

        bio::filtering_streambuf<bio::input> out;
        out.push(bio::gzip_compressor(bio::gzip_params(bio::gzip::best_speed)));
        out.push(s);
        bio::copy(out, compressed);
        
        compressed.seekg(0, std::ios::end);
        int size = compressed.tellg();
        compressed.seekg(0, std::ios::beg);

        std::string buf = compressed.str();

        int i;
        int t = 0;
        for (i = 0; i < size; i+=32768){

            int b = (size - i <= 32768) ? size - i : 32768;
           // std::cout << b  << " ";
            boost::system::error_code err;
            boost::asio::write(tcp_sock, buffer(buf.substr(i,b), b), err);
            //size_total += size;

            if (err){
                std::cout << err << std::endl;
            }
            t++;
        }

        std::cout << size  << " " << t << std::endl;

        

    }
    

/*
    */

    

    return 0;
}
