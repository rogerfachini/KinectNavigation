# Kinect Robot Navigation

Autonomous robot navigation using the Microsoft Kinect V1

## About

## Installation

This project uses libfreenect to interact with the Kinect camera. It must be built from source, using the directions below

### LibFreeNect

1) Install build dependencies

    ```
    sudo apt install git cmake build-essential libusb-1.0-0-dev cython python3 python3-dev python3-pip python3-numpy freeglut3-dev libxi-dev sudobuild-essential libxmu-dev
    ```

2) Clone libfreenect and build

    ```
    git clone https://github.com/OpenKinect/libfreenect
    cd libfreenect
    mkdir build
    cd build
    cmake .. -DBUILD_REDIST_PACKAGE=OFF -DBUILD_PYTHON3=ON
    make -j 8 install
    ```

3) Install python bindings
    
    ```
    cd ../wrappers/python
    sudo python3 setup.py install
    sudo pip3 install opencv-python
    ```

## Refrences

https://github.com/OpenKinect/libfreenect 

https://github.com/amiller/libfreenect-goodies

